package Darkmist;

use Moose;
use Moose::Autobox;
use MooseX::Params::Validate;
use Moose::Util::TypeConstraints;
use List::Util qw(first);
use Class::Load qw(:all);
use Try::Tiny;
use Data::Dumper;
use Darkmist::Role::Plugin;
use Config;

extends 'Reflex::Base';

has 'plugins' => (
    is      => 'rw',
    isa     => 'ArrayRef[Darkmist::Role::Plugin]',
    default => sub { [] },
);

# used for preventing more than one user interface from being loaded:
has 'ui' => (
    is      => 'rw',
    isa     => 'Bool',
    default => 0,
);


# Inspired by Dist::Zilla
sub plugins_with {
    my ($self, $role) = @_;

    $role =~ s/^::/Darkmist::Role::/;
    ($self->plugins->grep( sub { $_->does($role) } ))->flatten;
}

# Inspired by Dist::Zilla
sub plugin_named {
    my ($self, $name) = @_;

    if (my $plugin = first { $_->name eq $name } $self->plugins->flatten) {
	return $plugin;
    }

    # Search for a target alias (for command compatible):
    my @plugins = $self->plugins_with('::CommandHandler');
    first { $_->target eq $name } @plugins;
}

sub load_plugin {
    my ($self, $plugin, @args) = @_;

    my ($success, $error) = try_load_class($plugin);
    if (!$success) {
	return warn "the plugin: '$plugin' would not load:\n $error\n";
    }

    $plugin->does('Darkmist::Role::Plugin') or return warn
	"cannot load plugin '$plugin' because all plugins must consume ",
	"Darkmist::Role::Plugin. If you did not write this module and $plugin ",
	"is really a plugin for Darkmist, then please contact its author with ",
	"a friendly little whack.\n";

    $plugin = $plugin->new(core => $self, @args);

# try catch doesn't work here. I'll just leave this commented:
#    catch {
#	warn "There was an error constructing '$plugin' as a plugin. please ",
#	    "contact the author of this plugin with the following included ",
#	    "in your bug report:\n\n$_\n";
#    }

    $self->plugins->push($plugin);
    if (grep ('ui', ($plugin->tags)->flatten)) {
	if ($self->ui) {
	    return warn "a user interface has already been loaded.\n";
	}
	
        else {
	    $self->ui(1);
        }
    }

    $plugin->initialize;
    $plugin; # for success checking
}

# shorthand helper function for plugins:
sub dispatch {
    my ($self, $scope, $call, @args) = @_;

    my @plugins = $self->plugins_with($scope);
    foreach my $plugin (@plugins) {
	# TODO: Dispatch from here
	if ($plugin->can($call)) {
	    $plugin->$ {\ $call}(@args);
	}
    }
}

sub handle_command {
    my ($self, $input) = pos_validated_list(
	\@_,
	{ does => 'Darkmist' },
	{ isa  => 'Str'      },
    );

    my @apart = split ' ', $input;

    if (@apart == 1) {
	# TODO: plugin usage
	return;
    }

    my ($directive, $command) = @apart[0, 1];
    my @args = (); # default is no arguments
    @args = @apart[2 .. $#apart] unless @apart < 3;

    # This should be cleaned up. It is messy:
    if (my $plugin = $self->plugin_named($directive)) {
	if ($plugin->can($plugin->commands->{$command})) {
	    $plugin->${\ $plugin->commands->{$command}}(@args); #execute command
	}
    }
}

__PACKAGE__->meta->make_immutable;
