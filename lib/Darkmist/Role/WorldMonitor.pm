package Darkmist::Role::WorldMonitor;

use Moose::Role;


sub world_connected     {}

sub world_disconnected  {}

sub world_data          {}

1;
