package Darkmist::Role::Plugin;

use Moose::Role;

has 'name'  => (
    is      => 'rw',
    isa     => 'Str',
);

has 'target' => (
    is      => 'rw',
    isa     => 'Str',
);

has 'tags'  => (
    is      => 'rw',
    isa     => 'ArrayRef[Str]',
    default => sub { [] },
);

has 'core'  => (
    is      => 'ro',
    isa     => 'Darkmist',
    handles => 'Darkmist::API',
);

sub initialize {
    # Default: do nothing.
}

1;
