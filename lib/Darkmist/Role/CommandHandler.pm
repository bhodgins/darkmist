package Darkmist::Role::CommandHandler;

use Moose::Role;

has 'target' => (
    is  => 'rw',
    isa => 'Str',
);

has 'commands' => (
    is  => 'rw',
    isa => 'HashRef[Str]',
);

1;
