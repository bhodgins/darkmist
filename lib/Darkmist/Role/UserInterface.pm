package Darkmist::Role::UserInterface;

use Moose::Role;
use Moose::Autobox;

after BUILD => sub { shift->tags->push('ui') };

1;
