package Darkmist::API;
# A layer between the core and the plugins

use Moose::Role;


sub load_plugins {}

sub plugins_with {}

sub plugins_named {}

sub handle_command {}

sub dispatch {}

1;
