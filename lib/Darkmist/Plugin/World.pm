package Darkmist::Plugin::World;
# Darkmist must connect to something somehow-

use Moose;
use MooseX::Params::Validate;

with 'Darkmist::Role::Plugin';
with 'Darkmist::Role::CommandHandler';

has 'world' => (
    is  => 'rw',
    isa => 'Darkmist::Plugin::World::MUDClient',
);


sub initialize {
    my $self = shift;

    $self->name('Darkmist World Plugin');
    $self->target('world');

    $self->commands(
	{
	    'connect'    => 'connect',
	    'disconnect' => 'disconnect',
	    'add'        => 'add_world',
	    'remove'     => 'remove_world',
	    'quote'      => 'quote',
	}
    );
}

sub connect {
    my ($self, $host, $port) = pos_validated_list(
	\@_,
	{ does => 'Darkmist::Plugin::World' },
	{ isa  => 'Str'                     },
	{ isa  => 'Int',                    },
    );

    $self->world(
	Darkmist::Plugin::World::MUDClient->new(
	    address => $host,
	    port    => $port,
	    prox    => $self,
	)
      );
}

sub disconnect {

}

sub add_world {

}

sub remove_world {

}

sub quote {

}

__PACKAGE__->meta->make_immutable;

package Darkmist::Plugin::World::MUDClient;
# the actual object that connects

use Moose;
extends 'Reflex::Base';

has 'socket'  => (
    is        => 'rw',
    isa       => 'FileHandle',
);

has 'handle'  => (
    is        => 'rw',
    isa       => 'FileHandle',
    lazy      => 1,
    default   => sub { shift->socket }
);

has 'address' => (
    is        => 'ro',
    isa       => 'Str',
    required  => 1,
);

has 'port'    => (
    is        => 'ro',
    isa       => 'Int',
);

has 'active'  => (
    is        => 'ro',
    isa       => 'Bool',
    default   => 1,
);

has 'prox'   => (
    is        => 'ro',
    isa       => 'Darkmist::Plugin::World',
);

sub BUILD {} # To satisfy Moose?

sub on_connection {
    my ($self, $args) = @_;

    $self->prox->dispatch('::WorldMonitor', 'world_connected', $args)
}

sub on_error {
    my ($self, $args) = @_;

    print "error!\n";
}

sub on_socket_error {
    print "socket error\n";
}

sub on_socket_data {
    my ($self, $args) = @_;
    $self->prox->dispatch('::WorldMonitor', 'world_data', $args);
}

sub on_socket_closed {
    print "closed\n";
}

with 'Reflex::Role::Connecting' => {
    connector  => 'handle',
    address    => 'address',
    port       => 'port',
    cb_success => 'on_connection',
    cb_error   => 'on_error',
};

with 'Reflex::Role::Streaming'  => {
    handle     => 'socket',
    cb_data    => 'on_socket_data',
    cb_error   => 'on_socket_error',
    cb_closed  => 'on_socket_closed',
};

__PACKAGE__->meta->make_immutable;
