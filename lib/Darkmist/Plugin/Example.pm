package Darkmist::Plugin::Example;

use Moose; # We need this for sanity.

with 'Darkmist::Role::Plugin'; # REQUIRED. Will not load otherwise.
with 'Darkmist::Role::CommandHandler'; # This lets us handle commands.

# using BUILD is not recommended, but this is:
sub initialize {
    my $self = shift; # Self reference.

    # We will set up our properties here:
    $self->name('Example Plugin'); # Give your plugin a name!

    $self->target('example'); # alias for name, part of CommandHandler.
    # (Basically, commands starting with /example are directed here now. )

    $self->commands(
	{
	    'test' => 'test_command',
	}
    );
}

# Our command ends up here:
sub test_command {
    print "...And the example plugin goes.... EXAMPLE!\n";
}

__PACKAGE__->meta->make_immutable; # see Moose::Manual on perdoc. Not required.
