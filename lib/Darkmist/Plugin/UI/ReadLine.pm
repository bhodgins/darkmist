package Darkmist::Plugin::UI::ReadLine;

use Moose;
use Reflex 0.097; #0.096 has a bug that prevents this class from working
use Reflex::Interval;
use Reflex::Filehandle;
use Term::ReadLine;
use Reflex::Trait::Watched qw(watches);
use Time::HiRes;
extends 'Reflex::Base';

with 'Darkmist::Role::Plugin';
with 'Darkmist::Role::UserInterface';
with 'Darkmist::Role::WorldMonitor';

has 'term'   => (
    is       => 'rw',
    isa      => 'Term::ReadLine',
    default  => sub { Term::ReadLine->new('foo') },
);

has 'prompt' => (
    is       => 'rw',
    isa      => 'Str',
    default  => '>',
);

watches ticker => ( 
    isa   => 'Reflex::Interval',
    setup => { interval => 0.1, auto_repeat => 1 },
    #tag   => no_explosion,
);

# Called when Darkmist finishes initializing the plugin:
sub initialize {
    my $self = shift;

    $self->name('ReadLine User Interface');

    $self->term->event_loop(
	sub {
	    my $watcher = shift;
	    $watcher->next();
	},
	sub {
	    Reflex::Filehandle->new(
		handle => \*STDIN,
		rd     => 1,
	    );
	},
    );
} # TODO: power this plugin with a 65C816 or 65C02 compatible

sub on_ticker_tick {
    my $self = shift;

    my $input = $self->term->readline($self->prompt . ' ');
    if ($input =~ /^\//) {
	# command
	$self->core->handle_command(substr($input, 1));
    }
}

sub world_connected {
    print "connected to some world\n"; # works!
}

sub world_data {
    my ($self, $args) = @_;

    print $args->{octets};
}

1;
