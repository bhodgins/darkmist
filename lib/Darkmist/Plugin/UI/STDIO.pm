package Darkmist::Plugin::UI::STDIO;

use Moose;
use AnyEvent;
with 'Darkmist::Role::Plugin';
with 'Darkmist::Role::WorldMonitor';

has 'handle' => (
    is      => 'rw',
    isa     => 'FileHandle',
    default => sub {\*STDIN},
);

sub initialize {
    my $self = shift;

    $self->name('STDIO UI Plugin');

    
}

sub world_connected {
    my ($self, $args) = @_;

    use Data::Dumper;
    print Dumper $args;
}

sub world_disconnected {
    my ($self, $args) = @_;

    
}

sub world_data {
    my ($self, $args) = @_;

    print $args->{octets};
}

sub on_stdin {
    my ($self, $data) = @_;

    use Data::Dumper;
    print Dumper $data;
}

sub on_handle_closed {

}

sub on_handle_error {

}

__PACKAGE__->meta->make_immutable;
