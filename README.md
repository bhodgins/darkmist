# darkmist

... Is a MUD client that has a versatile plugin system. Even the user interface
itself is a plugin, allowing any user to become comfortable.
